from typing import Final

import pytest
from core import Instrument, RandomTribeFactory

MAX_MEMBERS: Final[int] = 5
MAX_INSTRUMENTS: Final[int] = 15
MAX_ENERGY: Final[int] = 50
MAX_COMPOSURE: Final[int] = 100

AVAILABLE_INSTRUMENTS: Final = (
    Instrument(name="Sing", required_energy=0, energy_cost=1, charm=1),
    Instrument(name="Dance", required_energy=20, energy_cost=2, charm=3),
    Instrument(name="Horn", required_energy=40, energy_cost=2, charm=4),
    Instrument(name="Maraca", required_energy=60, energy_cost=4, charm=6),
    Instrument(name="Didgeridoo", required_energy=80, energy_cost=4, charm=8),
)


@pytest.fixture(scope="session")
def random_tribe_factory() -> RandomTribeFactory:
    """Returns a preconfigured random tribe factory."""
    return RandomTribeFactory(
        max_members=MAX_MEMBERS,
        max_instruments=MAX_INSTRUMENTS,
        max_energy=MAX_ENERGY,
        max_composure=MAX_COMPOSURE,
        available_instruments=AVAILABLE_INSTRUMENTS,
    )
