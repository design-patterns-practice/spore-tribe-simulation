from typing import Final

import pytest
from core import (
    FurredTribeMember,
    Instrument,
    ITribeMember,
    RandomFurredTribeFactory,
    RandomTailedTribeFactory,
    RandomTribeFactory,
)
from hypothesis import given
from hypothesis.strategies import integers

TAIL_CHARM: Final[int] = 3
FUR_TYPE: Final = FurredTribeMember.Fur(name="Regular", charm_multiplier=2)


@pytest.fixture(scope="session")
def tailed_tribe_factory(
    random_tribe_factory: RandomTribeFactory,
) -> RandomTailedTribeFactory:
    """Returns a preconfigured random tailed tribe factory."""
    return RandomTailedTribeFactory(random_tribe_factory, tail_charm=TAIL_CHARM)


@pytest.fixture(scope="session")
def tailed_tribe_member(tailed_tribe_factory: RandomTailedTribeFactory) -> ITribeMember:
    """Returns a randomly generated tailed tribe member."""
    return tailed_tribe_factory.create_tribe().members[0]


@pytest.fixture(scope="session")
def furred_tribe_factory(
    random_tribe_factory: RandomTribeFactory,
) -> RandomFurredTribeFactory:
    """Returns a preconfigured random furred tribe factory."""
    return RandomFurredTribeFactory(random_tribe_factory, available_furs=[FUR_TYPE])


@pytest.fixture(scope="session")
def furred_tribe_member(furred_tribe_factory: RandomFurredTribeFactory) -> ITribeMember:
    """Returns a randomly generated furred tribe member."""
    return furred_tribe_factory.create_tribe().members[0]


@given(instrument_charm=integers(min_value=1))
def test_should_add_tail_charm(
    tailed_tribe_member: ITribeMember, instrument_charm: int
) -> None:
    test_instrument = Instrument(
        name="Testing",
        required_energy=tailed_tribe_member.energy,
        energy_cost=tailed_tribe_member.energy,
        charm=instrument_charm,
    )

    performance = tailed_tribe_member.get_performance(test_instrument)

    assert performance.name == "Testing"
    assert performance.emitted_charm == instrument_charm + TAIL_CHARM
    assert performance.consumed_energy == test_instrument.energy_cost


@given(instrument_charm=integers(min_value=1))
def test_should_multiply_by_fur_charm(
    furred_tribe_member: ITribeMember, instrument_charm: int
) -> None:
    test_instrument = Instrument(
        name="Test",
        required_energy=furred_tribe_member.energy,
        energy_cost=furred_tribe_member.energy,
        charm=instrument_charm,
    )

    performance = furred_tribe_member.get_performance(test_instrument)

    assert performance.name == "Test"
    assert performance.emitted_charm == instrument_charm * FUR_TYPE.charm_multiplier
    assert performance.consumed_energy == test_instrument.energy_cost
