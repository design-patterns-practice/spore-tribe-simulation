from dataclasses import dataclass, field

import pytest
from conftest import (
    AVAILABLE_INSTRUMENTS,
    MAX_COMPOSURE,
    MAX_ENERGY,
    MAX_INSTRUMENTS,
    MAX_MEMBERS,
)
from core import (
    DefaultPerformerSelector,
    DefaultWatcherSelector,
    Instrument,
    ITribeInteractor,
    LoggedTribeInteractor,
    Performance,
    RandomTribeFactory,
    Tribe,
    TribeInteractor,
    TribeMember,
    TribeView,
)


@dataclass
class StubLogger:
    guest_tribe: TribeView = field(init=False)
    host_tribe: TribeView = field(init=False)
    performance: Performance = field(init=False)
    hosts_charmed: bool = field(init=False)
    reset_count: int = field(default=0, init=False)

    def log_guest_tribe(self, tribe: TribeView) -> None:
        """Logs the characteristics of the guest tribe (including it's members)."""
        self.guest_tribe = tribe

    def log_host_tribe(self, tribe: TribeView) -> None:
        """Logs the characteristics of the host tribe (including it's members)."""
        self.host_tribe = tribe

    def log_performance(self, performance: Performance) -> None:
        """Logs the specified performance."""
        self.performance = performance

    def log_result(self, hosts_charmed: bool) -> None:
        """Logs the result of the performances between tribes."""
        self.hosts_charmed = hosts_charmed

    def log_reset(self) -> None:
        """Logs that the tribe interactor has been reset."""
        self.reset_count += 1


@dataclass
class StubTribeFactory:
    tribe: Tribe = field(init=False)

    def create_tribe(self) -> Tribe:
        """Creates a new tribe."""
        return self.tribe


@pytest.fixture
def logger() -> StubLogger:
    """Returns a stub logger, that will store data passed to it."""
    return StubLogger()


@pytest.fixture
def tribe_interactor(
    random_tribe_factory: RandomTribeFactory, logger: StubLogger
) -> ITribeInteractor:
    """Returns a TribeInteractor that uses a random tribe factory."""
    return LoggedTribeInteractor(
        TribeInteractor(tribe_factory=random_tribe_factory),
        logger=logger,
    )


def verify(*, tribe: TribeView) -> None:
    """Verifies properties of the specified tribe.

    Also checks that the stub logger has received the same instance of the tribe."""
    assert 0 < len(tribe.members) <= MAX_MEMBERS
    assert 0 < len(tribe.instruments) <= MAX_INSTRUMENTS

    for member in tribe:
        assert 0 < member.energy <= MAX_ENERGY
        assert 0 < member.composure <= MAX_COMPOSURE

    for instrument in tribe.instruments:
        assert instrument in AVAILABLE_INSTRUMENTS


def test_should_create_tribe_interactor(tribe_interactor: ITribeInteractor) -> None:
    assert tribe_interactor is not None


def test_should_evolve_guest_tribe(tribe_interactor: ITribeInteractor) -> None:
    guest_tribe = tribe_interactor.evolve_guests()
    verify(tribe=guest_tribe)


def test_should_evolve_host_tribe(tribe_interactor: ITribeInteractor) -> None:
    host_tribe = tribe_interactor.evolve_hosts()
    verify(tribe=host_tribe)


def test_should_evolve_different_guest_and_host_tribes(
    tribe_interactor: ITribeInteractor,
) -> None:
    assert tribe_interactor.evolve_guests() is not tribe_interactor.evolve_hosts()


def test_should_log_tribes(
    tribe_interactor: ITribeInteractor, logger: StubLogger
) -> None:
    assert tribe_interactor.evolve_guests() is logger.guest_tribe
    assert tribe_interactor.evolve_hosts() is logger.host_tribe


def test_should_start_performance(tribe_interactor: ITribeInteractor) -> None:
    guest_tribe = tribe_interactor.evolve_guests()
    host_tribe = tribe_interactor.evolve_hosts()

    instrument_names = [instrument.name for instrument in AVAILABLE_INSTRUMENTS]

    for performance in tribe_interactor.start_performances():
        assert performance.name in instrument_names
        assert performance.emitted_charm > 0
        assert performance.consumed_energy > 0

    if tribe_interactor.hosts_charmed():
        assert all(member.composure == 0 for member in host_tribe)
    else:
        assert any(member.composure > 0 for member in host_tribe)
        assert all(member.energy == 0 for member in guest_tribe)


def test_should_charm_hosts() -> None:
    tribe_factory = StubTribeFactory()
    tribe_interactor = TribeInteractor(tribe_factory=tribe_factory)

    tribe_factory.tribe = Tribe(
        members=[TribeMember(energy=1, composure=0)],
        instruments=[
            Instrument(name="Test", required_energy=1, energy_cost=1, charm=1)
        ],
        performer_selector=DefaultPerformerSelector(),
        watcher_selector=DefaultWatcherSelector(),
    )
    tribe_interactor.evolve_guests()

    tribe_factory.tribe = Tribe(
        members=[TribeMember(energy=0, composure=1)],
        instruments=[],
        performer_selector=DefaultPerformerSelector(),
        watcher_selector=DefaultWatcherSelector(),
    )
    tribe_interactor.evolve_hosts()

    for _ in tribe_interactor.start_performances():
        pass

    assert tribe_interactor.hosts_charmed()


def test_should_log_performances(
    tribe_interactor: ITribeInteractor, logger: StubLogger
) -> None:
    tribe_interactor.evolve_guests()
    tribe_interactor.evolve_hosts()

    for performance in tribe_interactor.start_performances():
        assert performance is logger.performance


def test_should_log_tribe_results(
    tribe_interactor: ITribeInteractor, logger: StubLogger
) -> None:
    tribe_interactor.evolve_guests()
    tribe_interactor.evolve_hosts()

    for _ in tribe_interactor.start_performances():
        pass

    assert tribe_interactor.hosts_charmed() is logger.hosts_charmed


def test_should_reset_tribe_interactor(
    tribe_interactor: ITribeInteractor, logger: StubLogger
) -> None:
    for reset_count in range(1, 5):
        tribe_interactor.evolve_guests()
        tribe_interactor.evolve_hosts()

        for _ in tribe_interactor.start_performances():
            pass

        tribe_interactor.reset()
        assert reset_count is logger.reset_count
