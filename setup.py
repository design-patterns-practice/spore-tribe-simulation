from setuptools import setup

setup(
    name="Spore Tribe Simulation",
    version="1.0.0",
    description="A simulation for tribal interaction for the game Spore.",
    package_dir={"": "app"},
    author="Zurab Mujirishvili",
    author_email="zmuji18@freeuni.edu.ge",
)
