from typing import Iterator, Protocol

from core.tribes import (
    ITribeInteractor,
    Performance,
    TribeInteractorDecorator,
    TribeView,
)


class ILogger(Protocol):
    def log_guest_tribe(self, tribe: TribeView) -> None:
        """Logs the characteristics of the guest tribe (including it's members)."""

    def log_host_tribe(self, tribe: TribeView) -> None:
        """Logs the characteristics of the host tribe (including it's members)."""

    def log_performance(self, performance: Performance) -> None:
        """Logs the specified performance."""

    def log_result(self, hosts_charmed: bool) -> None:
        """Logs the result of the performances between tribes."""

    def log_reset(self) -> None:
        """Logs that the tribe interactor has been reset."""


class LoggedTribeInteractor(TribeInteractorDecorator):
    def __init__(self, tribe_interactor: ITribeInteractor, *, logger: ILogger) -> None:
        """Creates a decorated tribe interactor that will log all tribe interactions."""
        super().__init__(tribe_interactor)
        self.__logger = logger

    def evolve_guests(self) -> TribeView:
        """Evolves a new guest tribe."""
        tribe = super().evolve_guests()
        self.__logger.log_guest_tribe(tribe)
        return tribe

    def evolve_hosts(self) -> TribeView:
        """Evolves a new host tribe."""
        tribe = super().evolve_hosts()
        self.__logger.log_host_tribe(tribe)
        return tribe

    def start_performances(self) -> Iterator[Performance]:
        """Starts a series of performances from the guest tribe.

        The performances last until the hosts are charmed, or the guests get tired."""
        for performance in super().start_performances():
            self.__logger.log_performance(performance)
            yield performance

    def hosts_charmed(self) -> bool:
        """Returns true if the host tribe has been charmed by the performances."""
        hosts_charmed = super().hosts_charmed()
        self.__logger.log_result(hosts_charmed)
        return hosts_charmed

    def reset(self) -> None:
        """Clears the current state of this tribe interactor."""
        super().reset()
        self.__logger.log_reset()
