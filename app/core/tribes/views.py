from typing import Any, Dict, Iterator, Protocol, Tuple

from core.tribes.tribe_member import Instrument


class TribeMemberView(Protocol):
    def as_string_data(self) -> Dict[str, Any]:
        """Returns a readable string representation of this tribe member."""

    @property
    def energy(self) -> int:
        """Returns the remaining energy of this tribe member."""

    @property
    def composure(self) -> int:
        """Returns the remaining composure of this tribe member."""


class TribeView(Protocol):
    def as_string_data(self) -> Dict[str, Any]:
        """Returns a readable string representation of this tribe (and it's members)."""

    @property
    def members(self) -> Tuple[TribeMemberView, ...]:
        """Returns views of the members of this tribe."""

    @property
    def instruments(self) -> Tuple[Instrument, ...]:
        """Returns the instruments possessed by this tribe."""

    def __iter__(self) -> Iterator[TribeMemberView]:
        """Returns an iterator over the views of the members of this tribe."""
