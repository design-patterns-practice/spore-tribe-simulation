from typing import Iterator, Optional, Protocol

from core.tribes.factory import ITribeFactory
from core.tribes.tribe import (
    NoComposureLeftError,
    NoPerformancesLeftError,
    Performance,
    Tribe,
)
from core.tribes.views import TribeView


class ITribeInteractor(Protocol):
    def evolve_guests(self) -> TribeView:
        """Evolves a new guest tribe."""

    def evolve_hosts(self) -> TribeView:
        """Evolves a new host tribe."""

    def start_performances(self) -> Iterator[Performance]:
        """Starts a series of performances from the guest tribe.

        The performances last until the hosts are charmed, or the guests get tired."""

    def hosts_charmed(self) -> bool:
        """Returns true if the host tribe has been charmed by the performances."""

    def reset(self) -> None:
        """Clears the current state of this tribe interactor."""


class TribeInteractor:
    def __init__(self, *, tribe_factory: ITribeFactory) -> None:
        self.__tribe_factory = tribe_factory

        self.__guest_tribe: Optional[Tribe] = None
        self.__host_tribe: Optional[Tribe] = None
        self.__hosts_charmed: Optional[bool] = None

    def evolve_guests(self) -> TribeView:
        """Evolves a new guest tribe."""
        assert self.__guest_tribe is None, "A guest tribe has already been evolved."

        self.__guest_tribe = self.__tribe_factory.create_tribe()
        return self.__guest_tribe

    def evolve_hosts(self) -> TribeView:
        """Evolves a new host tribe."""
        assert self.__host_tribe is None, "A host tribe has already been evolved."

        self.__host_tribe = self.__tribe_factory.create_tribe()
        return self.__host_tribe

    def start_performances(self) -> Iterator[Performance]:
        """Starts a series of performances from the guest tribe.

        The performances last until the hosts are charmed, or the guests get tired."""
        assert self.__guest_tribe is not None, "A guest tribe must be evolved first."
        assert self.__host_tribe is not None, "A host tribe must be evolved first."

        try:
            while True:
                performance = self.__guest_tribe.next_performance()
                yield performance
                self.__host_tribe.watch_performance(performance)

        except NoPerformancesLeftError:
            self.__hosts_charmed = False
        except NoComposureLeftError:
            self.__hosts_charmed = True

    def hosts_charmed(self) -> bool:
        """Returns true if the host tribe has been charmed by the performances."""
        assert self.__hosts_charmed is not None, "The guests have not yet performed."

        return self.__hosts_charmed

    def reset(self) -> None:
        """Clears the current state of this tribe interactor."""
        self.__guest_tribe = None
        self.__host_tribe = None
        self.__hosts_charmed = None


class TribeInteractorDecorator:
    def __init__(self, tribe_interactor: ITribeInteractor) -> None:
        """Creates a default decorator for the specified tribe interactor."""
        self.__tribe_interactor = tribe_interactor

    def evolve_guests(self) -> TribeView:
        """Evolves a new guest tribe."""
        return self.__tribe_interactor.evolve_guests()

    def evolve_hosts(self) -> TribeView:
        """Evolves a new host tribe."""
        return self.__tribe_interactor.evolve_hosts()

    def start_performances(self) -> Iterator[Performance]:
        """Starts a series of performances from the guest tribe.

        The performances last until the hosts are charmed, or the guests get tired."""
        return self.__tribe_interactor.start_performances()

    def hosts_charmed(self) -> bool:
        """Returns true if the host tribe has been charmed by the performances."""
        return self.__tribe_interactor.hosts_charmed()

    def reset(self) -> None:
        """Clears the current state of this tribe interactor."""
        return self.__tribe_interactor.reset()
