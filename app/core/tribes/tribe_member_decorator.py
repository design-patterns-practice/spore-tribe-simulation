from dataclasses import dataclass
from typing import Any, Dict

from core.tribes.tribe_member import Instrument, ITribeMember, Performance


class TribeMemberDecorator:
    def __init__(self, tribe_member: ITribeMember) -> None:
        """Creates a default decorator for a tribe member."""
        self.__tribe_member = tribe_member

    def has_composure(self, performance: Performance) -> bool:
        """Returns true if this tribe member has composure left for the specified performance."""
        return self.__tribe_member.has_composure(performance)

    def watch_performance(self, performance: Performance) -> None:
        """Watches the specified performance."""
        self.__tribe_member.watch_performance(performance)

    def has_performance(self, instrument: Instrument) -> bool:
        """Returns true if this tribe member can perform with the specified instrument."""
        return self.__tribe_member.has_performance(instrument)

    def get_performance(self, instrument: Instrument) -> Performance:
        """Returns the performance of this tribe member with the specified instrument."""
        return self.__tribe_member.get_performance(instrument)

    def flush_remaining_composure(self) -> None:
        """Flushes out any remaining composure from this tribe member."""
        return self.__tribe_member.flush_remaining_composure()

    def flush_remaining_energy(self) -> None:
        """Flushes out any remaining energy from this tribe member."""
        return self.__tribe_member.flush_remaining_energy()

    def as_string_data(self) -> Dict[str, Any]:
        """Returns a readable string representation of this tribe member."""
        return self.__tribe_member.as_string_data()

    @property
    def energy(self) -> int:
        """Returns the remaining energy of this tribe member."""
        return self.__tribe_member.energy

    @property
    def composure(self) -> int:
        """Returns the remaining composure of this tribe member."""
        return self.__tribe_member.composure


class TailedTribeMember(TribeMemberDecorator):
    def __init__(self, tribe_member: ITribeMember, *, tail_charm: int) -> None:
        super().__init__(tribe_member)
        self.__tail_charm = tail_charm

    def get_performance(self, instrument: Instrument) -> Performance:
        """Returns the performance of this tribe member with the specified instrument."""
        base_performance = super().get_performance(instrument)
        return Performance(
            name=instrument.name,
            emitted_charm=base_performance.emitted_charm + self.__tail_charm,
            consumed_energy=base_performance.consumed_energy,
        )

    def as_string_data(self) -> Dict[str, Any]:
        """Returns a readable string representation of this tribe member."""
        tribe_member = super().as_string_data()
        tribe_member.setdefault("With", {})["Tail Charm"] = self.__tail_charm
        return tribe_member


class FurredTribeMember(TribeMemberDecorator):
    @dataclass(frozen=True)
    class Fur:
        name: str
        charm_multiplier: int

    def __init__(self, tribe_member: ITribeMember, *, fur: Fur) -> None:
        super().__init__(tribe_member)
        self.__fur = fur

    def get_performance(self, instrument: Instrument) -> Performance:
        """Returns the performance of this tribe member with the specified instrument."""
        base_performance = super().get_performance(instrument)
        return Performance(
            name=instrument.name,
            emitted_charm=base_performance.emitted_charm * self.__fur.charm_multiplier,
            consumed_energy=base_performance.consumed_energy,
        )

    def as_string_data(self) -> Dict[str, Any]:
        """Returns a readable string representation of this tribe member."""
        tribe_member = super().as_string_data()
        tribe_member.setdefault("With", {})["Fur"] = self.fur.name
        return tribe_member

    @property
    def fur(self) -> Fur:
        return self.__fur
