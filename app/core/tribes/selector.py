from typing import Optional, Protocol, Sequence, Tuple, Union

from core.tribes.tribe_member import Instrument, ITribeMember, Performance

PerformancePair = Union[Tuple[ITribeMember, Instrument], Tuple[None, None]]


class IPerformerSelector(Protocol):
    def get_performer(
        self, *, members: Sequence[ITribeMember], instruments: Sequence[Instrument]
    ) -> PerformancePair:
        """Returns the tribe member that should perform next and the instrument that should be used.

        None is returned if a performer could not be chosen."""


class IWatcherSelector(Protocol):
    def get_watcher(
        self, *, members: Sequence[ITribeMember], performance: Performance
    ) -> Optional[ITribeMember]:
        """Returns the tribe member that should watch the next performance."""


class DefaultPerformerSelector:
    @staticmethod
    def get_performer(
        *, members: Sequence[ITribeMember], instruments: Sequence[Instrument]
    ) -> PerformancePair:
        """Returns the tribe member that should perform next and the instrument that should be used.

        None is returned if a performer could not be chosen."""
        for member in members:
            for instrument in instruments:
                if member.has_performance(instrument):
                    return member, instrument

        return None, None


class DefaultWatcherSelector:
    @staticmethod
    def get_watcher(
        *, members: Sequence[ITribeMember], performance: Performance
    ) -> Optional[ITribeMember]:
        """Returns the tribe member that should watch the next performance."""
        for member in members:
            if member.has_composure(performance):
                return member

        return None
