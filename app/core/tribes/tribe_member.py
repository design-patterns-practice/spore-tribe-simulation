from dataclasses import dataclass
from typing import Any, Dict, Protocol


@dataclass(frozen=True)
class Instrument:
    name: str
    required_energy: int
    energy_cost: int
    charm: int

    def as_string_data(self) -> Dict[str, Any]:
        """Returns a readable string representation of this instrument."""
        return {
            "Name": self.name,
            "Required Energy": self.required_energy,
            "Energy Cost": self.energy_cost,
            "Charm": self.charm,
        }


@dataclass(frozen=True)
class Performance:
    name: str
    emitted_charm: int
    consumed_energy: int

    def as_string_data(self) -> Dict[str, Any]:
        """Returns a readable string representation of this performance."""
        return {
            "Name": self.name,
            "Emitted Charm": self.emitted_charm,
            "Consumed Energy": self.consumed_energy,
        }


class ITribeMember(Protocol):
    def has_composure(self, performance: Performance) -> bool:
        """Returns true if this tribe member has composure left for the specified performance."""

    def watch_performance(self, performance: Performance) -> None:
        """Watches the specified performance."""

    def has_performance(self, instrument: Instrument) -> bool:
        """Returns true if this tribe member can perform with the specified instrument."""

    def get_performance(self, instrument: Instrument) -> Performance:
        """Returns the performance of this tribe member with the specified instrument."""

    def flush_remaining_composure(self) -> None:
        """Flushes out any remaining composure from this tribe member."""

    def flush_remaining_energy(self) -> None:
        """Flushes out any remaining energy from this tribe member."""

    def as_string_data(self) -> Dict[str, Any]:
        """Returns a readable string representation of this tribe member."""

    @property
    def energy(self) -> int:
        """Returns the remaining energy of this tribe member."""

    @property
    def composure(self) -> int:
        """Returns the remaining composure of this tribe member."""


class TribeMember:
    def __init__(self, *, energy: int, composure: int) -> None:
        self.__energy = energy
        self.__composure = composure

    def has_composure(self, performance: Performance) -> bool:
        """Returns true if this tribe member has composure left for the specified performance."""
        return self.__composure >= performance.emitted_charm

    def watch_performance(self, performance: Performance) -> None:
        """Watches the specified performance."""
        self.__composure -= performance.emitted_charm

    def has_performance(self, instrument: Instrument) -> bool:
        """Returns true if this tribe member can perform with the specified instrument."""
        return (
            self.__energy >= instrument.required_energy
            and self.__energy >= instrument.energy_cost
        )

    def get_performance(self, instrument: Instrument) -> Performance:
        """Returns the performance of this tribe member with the specified instrument."""
        assert self.has_performance(
            instrument
        ), f"This tribe member cannot perform with the instrument {instrument}."

        self.__energy -= instrument.energy_cost

        return Performance(
            name=instrument.name,
            emitted_charm=instrument.charm,
            consumed_energy=instrument.energy_cost,
        )

    def flush_remaining_composure(self) -> None:
        """Flushes out any remaining composure from this tribe member."""
        self.__composure = 0

    def flush_remaining_energy(self) -> None:
        """Flushes out any remaining energy from this tribe member."""
        self.__energy = 0

    def as_string_data(self) -> Dict[str, Any]:
        """Returns a readable string representation of this tribe member."""
        return {
            "Energy": self.__energy,
            "Composure": self.__composure,
        }

    @property
    def energy(self) -> int:
        """Returns the remaining energy of this tribe member."""
        return self.__energy

    @property
    def composure(self) -> int:
        """Returns the remaining composure of this tribe member."""
        return self.__composure
