from random import choice, randint
from typing import Protocol, Sequence

from core.tribes.selector import (
    DefaultPerformerSelector,
    DefaultWatcherSelector,
    IPerformerSelector,
    IWatcherSelector,
)
from core.tribes.tribe import Tribe
from core.tribes.tribe_member import Instrument, TribeMember
from core.tribes.tribe_member_decorator import FurredTribeMember, TailedTribeMember


class ITribeFactory(Protocol):
    def create_tribe(self) -> Tribe:
        """Creates a new tribe."""


class RandomTribeFactory:
    def __init__(
        self,
        *,
        max_members: int,
        max_instruments: int,
        max_energy: int,
        max_composure: int,
        available_instruments: Sequence[Instrument],
        performer_selector: IPerformerSelector = DefaultPerformerSelector(),
        watcher_selector: IWatcherSelector = DefaultWatcherSelector()
    ) -> None:
        self.__max_members = max_members
        self.__max_instruments = max_instruments
        self.__max_energy = max_energy
        self.__max_composure = max_composure
        self.__available_instruments = available_instruments
        self.__performer_selector = performer_selector
        self.__watcher_selector = watcher_selector

    def create_tribe(self) -> Tribe:
        """Creates a new tribe."""
        members = [
            TribeMember(
                energy=randint(1, self.__max_energy),
                composure=randint(1, self.__max_composure),
            )
            for _ in range(randint(1, self.__max_members))
        ]

        instruments = [
            choice(self.__available_instruments)
            for _ in range(randint(1, self.__max_instruments))
        ]

        return Tribe(
            members,
            instruments=instruments,
            performer_selector=self.__performer_selector,
            watcher_selector=self.__watcher_selector,
        )


class RandomTailedTribeFactory:
    def __init__(
        self,
        tribe_factory: ITribeFactory,
        *,
        tail_charm: int,
        always_tailed: bool = True
    ) -> None:
        self.__tribe_factory = tribe_factory
        self.__tail_charm = tail_charm
        self.__always_tailed = always_tailed

    def create_tribe(self) -> Tribe:
        """Creates a new tribe."""
        tribe = self.__tribe_factory.create_tribe()
        tribe.members = tuple(
            TailedTribeMember(member, tail_charm=self.__tail_charm)
            if choice([True, False]) or self.__always_tailed
            else member
            for member in tribe.members
        )
        return tribe


class RandomFurredTribeFactory:
    def __init__(
        self,
        tribe_factory: ITribeFactory,
        *,
        available_furs: Sequence[FurredTribeMember.Fur],
        always_furred: bool = True
    ) -> None:
        self.__tribe_factory = tribe_factory
        self.__available_furs = available_furs
        self.__always_furred = always_furred

    def create_tribe(self) -> Tribe:
        """Creates a new tribe."""
        tribe = self.__tribe_factory.create_tribe()
        tribe.members = tuple(
            FurredTribeMember(member, fur=choice(self.__available_furs))
            if choice([True, False]) or self.__always_furred
            else member
            for member in tribe.members
        )
        return tribe
