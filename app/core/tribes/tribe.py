from typing import Any, Dict, Iterator, Sequence, Tuple

from core.tribes.selector import IPerformerSelector, IWatcherSelector
from core.tribes.tribe_member import Instrument, ITribeMember, Performance


class NoPerformancesLeftError(Exception):
    """Thrown whenever a tribe has no more energy left and is asked to perform."""


class NoComposureLeftError(Exception):
    """Thrown whenever a tribe has no more composure left and is asked to watch a performance."""


class Tribe:
    def __init__(
        self,
        members: Sequence[ITribeMember],
        *,
        instruments: Sequence[Instrument],
        performer_selector: IPerformerSelector,
        watcher_selector: IWatcherSelector,
    ) -> None:
        self.__members = tuple(members)
        self.__instruments = tuple(instruments)
        self.__performer_selector = performer_selector
        self.__watcher_selector = watcher_selector

    def __iter__(self) -> Iterator[ITribeMember]:
        return iter(self.members)

    def next_performance(self) -> Performance:
        """Returns the next performance from one of the members of this tribe.

        :raises NoPerformancesLeftError if all tribe members are out of energy."""
        performer, instrument = self.__performer_selector.get_performer(
            members=self.members, instruments=self.instruments
        )

        if performer is not None:
            assert (
                instrument is not None
            ), "The instrument must be chosen if a performer is chosen."

            return performer.get_performance(instrument)

        for member in self:
            member.flush_remaining_energy()

        raise NoPerformancesLeftError("This tribe has no more performances left.")

    def watch_performance(self, performance: Performance) -> None:
        """Watches the specified performance.

        :raises NoComposureLeftError if all tribe members are out of composure."""
        # NOTE: This is written so that all the charm emitted by a performance will be distributed to one
        # host tribe member. The requirements say that I can do this however I like, so I chose this way.
        # Alternatively, someone could implement a strategy pattern here to distribute the emitted charm as
        # they wish, but I think it's pretty overkill for now.
        watcher = self.__watcher_selector.get_watcher(
            members=self.members, performance=performance
        )

        if watcher is not None:
            watcher.watch_performance(performance)

            if all(member.composure == 0 for member in self):
                raise NoComposureLeftError("This tribe has no more composure left.")
            else:
                return

        for member in self:
            member.flush_remaining_composure()

        raise NoComposureLeftError("This tribe has no more composure left.")

    def as_string_data(self) -> Dict[str, Any]:
        """Returns a readable string representation of this tribe (and it's members)."""
        members = [{"Tribe Member": member.as_string_data()} for member in self.members]
        instruments = [
            {"Instrument": instrument.as_string_data()}
            for instrument in self.instruments
        ]
        return {"Members": members, "Instruments": instruments}

    @property
    def members(self) -> Tuple[ITribeMember, ...]:
        """Returns views of the members of this tribe."""
        return self.__members

    @members.setter
    def members(self, members: Tuple[ITribeMember, ...]) -> None:
        """Sets a new sequence of members for this tribe."""
        self.__members = members

    @property
    def instruments(self) -> Tuple[Instrument, ...]:
        """Returns the instruments possessed by this tribe."""
        return self.__instruments
