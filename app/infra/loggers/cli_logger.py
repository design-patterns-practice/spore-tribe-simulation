from typing import Any, Final

import yaml
from core import Performance, TribeView

LEVEL_INDENT: Final[int] = 4


def format_data(data: Any) -> Any:
    """Formats the specified data to be easily readable."""
    return yaml.dump(data, sort_keys=False, indent=LEVEL_INDENT)


class CliLogger:
    @staticmethod
    def log_guest_tribe(tribe: TribeView) -> None:
        """Logs the characteristics of the guest tribe (including it's members)."""
        tribe_data = {"Tribe": tribe.as_string_data()}
        print(f"Evolved Guest {format_data(tribe_data)}\n")

    @staticmethod
    def log_host_tribe(tribe: TribeView) -> None:
        """Logs the characteristics of the host tribe (including it's members)."""
        """Logs the characteristics of the guest tribe (including it's members)."""
        tribe_data = {"Tribe": tribe.as_string_data()}
        print(f"Evolved Host {format_data(tribe_data)}\n")

    @staticmethod
    def log_performance(performance: Performance) -> None:
        """Logs the specified performance."""
        performance_data = {"Performance": performance.as_string_data()}
        print(f"Guest {format_data(performance_data)}\n")

    @staticmethod
    def log_result(hosts_charmed: bool) -> None:
        """Logs the result of the performances between tribes."""
        if hosts_charmed:
            print("Guests have charmed hosts.")
        else:
            print("Hosts have disappointed guests.")

    @staticmethod
    def log_reset() -> None:
        """Logs that the tribe interactor has been reset."""
        print("Tribe interactor state has been reset.")
