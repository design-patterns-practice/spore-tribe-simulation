from core import ITribeInteractor


class Cli:
    def __init__(
        self, *, tribe_interactor: ITribeInteractor, simulation_count: int
    ) -> None:
        self.__tribe_interactor = tribe_interactor
        self.__simulation_count = simulation_count

    def run(self) -> None:
        """Runs a simple simulation of tribe interactions."""
        charm_count = 0

        for simulation_index in range(1, self.__simulation_count + 1):
            print(f"\nStarting tribe simulation #{simulation_index}...\n")

            self.__tribe_interactor.evolve_guests()
            self.__tribe_interactor.evolve_hosts()

            for _ in self.__tribe_interactor.start_performances():
                pass

            if self.__tribe_interactor.hosts_charmed():
                charm_count += 1

            print(f"Tribe simulation #{simulation_index} finished.")

            self.__tribe_interactor.reset()

        print(f"\nAll {self.__simulation_count} tribe simulation are finished.")
        print(f"Hosts were charmed a total of {charm_count} times.")

    def __call__(self) -> None:
        """Same as .run()."""
        self.run()
