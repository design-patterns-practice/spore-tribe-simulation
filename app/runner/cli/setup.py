from typing import Callable, Final

from core import (
    FurredTribeMember,
    Instrument,
    LoggedTribeInteractor,
    RandomFurredTribeFactory,
    RandomTailedTribeFactory,
    RandomTribeFactory,
    TribeInteractor,
)
from infra import Cli, CliLogger

MAX_MEMBERS: Final[int] = 5
MAX_INSTRUMENTS: Final[int] = 15
MAX_ENERGY: Final[int] = 50
MAX_COMPOSURE: Final[int] = 100

AVAILABLE_INSTRUMENTS: Final = (
    Instrument(name="Sing", required_energy=0, energy_cost=1, charm=1),
    Instrument(name="Dance", required_energy=20, energy_cost=2, charm=3),
    Instrument(name="Horn", required_energy=40, energy_cost=2, charm=4),
    Instrument(name="Maraca", required_energy=60, energy_cost=4, charm=6),
    Instrument(name="Didgeridoo", required_energy=80, energy_cost=4, charm=8),
)

TAIL_CHARM: Final[int] = 3

AVAILABLE_FURS: Final = (
    FurredTribeMember.Fur(name="Regular", charm_multiplier=2),
    FurredTribeMember.Fur(name="Striped", charm_multiplier=4),
    FurredTribeMember.Fur(name="Dotted", charm_multiplier=6),
)

SIMULATION_COUNT: Final[int] = 100


def setup() -> Callable[[], None]:
    """Returns a configured application that can be run directly."""
    return Cli(
        tribe_interactor=LoggedTribeInteractor(
            TribeInteractor(
                tribe_factory=RandomFurredTribeFactory(
                    RandomTailedTribeFactory(
                        RandomTribeFactory(
                            max_members=MAX_MEMBERS,
                            max_instruments=MAX_INSTRUMENTS,
                            max_energy=MAX_ENERGY,
                            max_composure=MAX_COMPOSURE,
                            available_instruments=AVAILABLE_INSTRUMENTS,
                        ),
                        tail_charm=TAIL_CHARM,
                        always_tailed=False,
                    ),
                    available_furs=AVAILABLE_FURS,
                    always_furred=False,
                )
            ),
            logger=CliLogger(),
        ),
        simulation_count=SIMULATION_COUNT,
    )
